 document.addEventListener("DOMContentLoaded", function (event) {
    var list = document.createElement('ul');
    var friends = ['Hang','Nga','Toan','Lan'];
    friends.forEach(function(name){
      var person = document.createElement('li');
      var t = document.createTextNode(name);
      person.appendChild(t);
      list.appendChild(person);
    });
    document.querySelector('.content').append(list);
    document.querySelector('.content').style.cssText = 'padding: 1em'
    document.querySelector('.content').style.backgroundColor='blue';
    document.querySelector('.content').style.color='red';
});