 document.addEventListener("DOMContentLoaded", function (event) {
    var list = document.createElement('ul');
    var friends = ['Hang','Nga','Toan','Lan'];
    friends.forEach(function(name){
      var person = document.createElement('li');
      var t = document.createTextNode(name);
      person.appendChild(t);
      list.appendChild(person);
    });
    document.querySelector('.content').append(list);
});