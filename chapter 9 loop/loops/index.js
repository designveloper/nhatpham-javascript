document.addEventListener("DOMContentLoaded", function (event) {
  var peoples = [
    { name: 'Pham Hoang Minh Nhat', age: 22, address: '89 ngo quyen' },
    { name: 'Pham Hoang Minh A', age: 22, address: '89 ngo quyen' },
    { name: 'Pham Hoang Minh B', age: 22, address: '89 ngo quyen' },
    { name: 'Pham Hoang Minh C', age: 22, address: '89 ngo quyen' },
    { name: 'Pham Hoang Minh D', age: 22, address: '89 ngo quyen' },
  ]
  console.log('user for loop');
  for (var i = 0; i < peoples.length; i++)
    console.log('name: ' + peoples[i].name + ' age: ' + peoples[i].age);
  console.log('use while loop') 

  console.log('use forEach');

  peoples.forEach(function (people) {
    console.log('name: ' + people.name + ' age: ' + people.age);
  });
  console.log('use map in ES6');
  peoples.map((people) => {
    console.log('name: ' + people.name + ' age: ' + people.age);
  });
});