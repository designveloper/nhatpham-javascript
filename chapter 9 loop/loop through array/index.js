document.addEventListener("DOMContentLoaded", function (event) {
  var links = document.querySelectorAll('.link');
  links.forEach(function (link) {
    if (!link.hasAttribute('target'))
      link.setAttribute('target', '_blank')
  }, this);
});