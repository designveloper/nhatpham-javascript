var colors = ['green', 'blue', 'white', 'black'];
//remove the last item in array
colors.pop();
// add pink in the last position
colors.push('pink');
// remove the first item in array
colors.shift();
// insert gray color to the first position of array
colors.unshift('gray');
console.log(colors);
// reverse array
console.log(colors.reverse());
// find index of blue
console.log(colors.indexOf('blue'));
// join all item to string with '~'
console.log(colors.join('~'));