 document.addEventListener("DOMContentLoaded", function (event) {
   const CTA = document.querySelector('a');
   const ALERT = document.querySelector('.main-title');
   CTA.classList.remove('hide');
   ALERT.classList.add('hide');
   function reveal(){
     CTA.classList.toggle('hide');
     ALERT.classList.toggle('hide');
   }
    CTA.onclick = reveal;
});