var course = new Object();

course = {
  title: 'JavaScript Essential Traning',
  instructor: 'Mr Nhat',
  level: 1,
  published: true,
  views: 0,
  updateViews: function () {
    return ++course.views;
  }
}
console.log(course.views);
course.updateViews();
console.log(course.views);