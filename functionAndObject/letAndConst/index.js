let email = 'nhathoangminhpham@gmail.com';
let email = 'nhatphm@dgroup.com' // this makes error because 'let' can not be redefined;

function logScope() {
  var localVar = 3;
  if (localVar) {
    let localVar = 'Pham hoang minh nhat';
    console.log('nested localvar', localVar);
  }
  console.log('logscope localvar', localVar);
}
logScope();

const PI = 3.14;
// const can not be redefined
const PI = 3.141233; // error
// const can not be assigned
PI = 3.1423322;

