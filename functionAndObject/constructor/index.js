function Course(title, instructor, level, published, views) {
  this.title = title;
  this.instructor = instructor;
  this.level = level;
  this.published = published;
  this.views = views;
  this.updateViews = function () {
    return ++this.views;
  };
}
var course1 = new Course('JavaScript Essential Traning', 'Mr Nhat', 3, true, 33);
course1.updateViews();
console.log(course1.views);