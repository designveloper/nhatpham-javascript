var hello ='starting function'
var findTheBiggestFraction = (function (a, b) {
  // we can use hello because hello is global variale
  console.log(hello);
  var result;
  a > b ? result = a : result = b;
  return result;
})(3 / 4, 2 / 5);
console.log(findTheBiggestFraction);
console.log(result); // this fail because result is declared in body of function;